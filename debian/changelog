libimage-librsvg-perl (0.07-10) unstable; urgency=medium

  * Team upload.
  * gcc-14.patch: new: fix generic functions declaration. (Closes: #1075173)
  * Declare compliance with Debian Policy 4.7.0.
  * d/*lintian-overrides: refresh mismatched override.

 -- Étienne Mollier <emollier@debian.org>  Sun, 21 Jul 2024 20:51:42 +0200

libimage-librsvg-perl (0.07-9) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update path in lintian override.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Antonio Terceiro ]
  * Remove myself from Uploaders: field. Thanks a lot for the folks at the
    Perl team who have been keeping this package updated for several years
    after I stopped working on it.

  [ Lucas Kanashiro ]
  * Apply Antonio's patch removing himself from Uploaders (Closes: #872161)

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove trailing whitespace from debian/*.
  * Drop override_dh_compress.
    debhelper in compat level 12 doesn't compress examples anymore.
  * debian/rules: remove manual handling of compiler flags.
  * Set bindnow linker flag in debian/rules.
  * debian/control: update Build-Depends for cross builds.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Remove $Config{vendorarch} from debian/rules. Causes problems with
    cross-building, and can be replaced by `find … -delete'.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Wed, 03 Nov 2021 16:57:09 +0100

libimage-librsvg-perl (0.07-8) unstable; urgency=medium

  * Team upload.
  * Use $Config{vendorarch} to get library path in debian/rules.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Aug 2014 18:59:00 +0200

libimage-librsvg-perl (0.07-7) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Declare compliance with Debian Policy 3.9.5
  * Fix typo in hash key, add Forwarded URL to init_rsvg.patch
  * Drop versionless dependency on build-essential dpkg-dev
  * Use a link instead of two identical files
  * Make sure even CPPFLAGS are passed, and add a linitan override for the
    false positive

 -- Florian Schlichting <fsfs@debian.org>  Thu, 24 Apr 2014 22:03:46 +0200

libimage-librsvg-perl (0.07-6) unstable; urgency=low

  * Team upload.
  * debian/control: Convert Vcs-* fields to Git.
  * Fix format-security compiler warnings. (Closes: #643428)
    + new patch: format-security.diff
  * Bumped Standards-Version to 3.9.2.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 02 Oct 2011 11:14:52 +0200

libimage-librsvg-perl (0.07-5) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Split out the patch from 0.07-4 into a separate patch.
  * debian/watch: use dist-based URL.
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * debian/control: change section to perl.
  * Remove Conflicts:/Replaces: libimage-rsvg-perl.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Peter Pentchev ]
  * Remove /usr/lib/perl5/Image/examples.pl.  Closes: #611120
  * Also remove a duplicate upstream example file.
  * Convert to the 3.0 (quilt) source format.
  * Add myself to Uploaders.
  * Reword the package description.
  * Refresh the copyright file:
    - refer to "Debian systems" and the GPL-1 file instead of the symlink
    - update it to the latest revision of the DEP 5 candidate format
    - add my copyright notice
  * Bump the debhelper compatibility level to 8 and minimize the rules file
    using override rules.
  * Use dpkg-buildflags from dpkg-dev 1.15.7 to obtain the default values
    for CPPFLAGS, CFLAGS and LDFLAGS.
  * Bump Standards-Version to 3.9.1 with no further changes.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 27 Jan 2011 00:30:29 +0200

libimage-librsvg-perl (0.07-4) unstable; urgency=low

  * Initializing RSVG at the XS constructor, fixing FTBFS. Thanks to Niko
    Tyni for yet another great patch! (Closes: #430805)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 03 Jul 2007 13:55:39 -0500

libimage-librsvg-perl (0.07-3) unstable; urgency=low

  * Clean up unnecessary library dependencies.
  * Honor 'noopt' in DEB_BUILD_OPTIONS.
  * Upgrade to debhelper compatibility level 5.
  * Upgrade to Standards-Version 3.7.2. No changes needed.
  * Don't ignore the return value of 'make distclean'.

 -- Niko Tyni <ntyni@iki.fi>  Tue, 17 Oct 2006 21:44:36 +0300

libimage-librsvg-perl (0.07-2) unstable; urgency=low

  * Drop dependencies from experimental

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 20 Mar 2006 14:59:42 +0100

libimage-librsvg-perl (0.07-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 15 Mar 2006 15:07:41 +0100

libimage-librsvg-perl (0.06-1) unstable; urgency=low

  * New upstream release (closes: #356239)
  * debian/control - added me to Uploaders

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 13 Mar 2006 19:31:17 +0100

libimage-librsvg-perl (0.05-2) unstable; urgency=low

  * Add missing dependencies on shared libraries (${shlibs:Depends}).

 -- Niko Tyni <ntyni@iki.fi>  Tue, 13 Dec 2005 21:31:25 +0000

libimage-librsvg-perl (0.05-1) unstable; urgency=low

  * Repackaging under the right name, conflicting & replacing with old package
    name libimage-rsvg-perl as said in Developer's Reference, 5.9.3
    (Closes: #324419)
  * libimage-rsvg-perl must be removed, after this one enters the archive I'll
    request the removal

 -- Antonio S. de A. Terceiro <asaterceiro@inf.ufrgs.br>  Mon, 22 Aug 2005 10:44:02 -0300
